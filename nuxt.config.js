const pkg = require('./package')

module.exports = {
  mode: 'universal',

  /*
  ** Headers of the page
  */
  head: {
    title: 'Gyru',
    meta: [
      { charset: 'utf-8' },
      {
        name: 'viewport',
        content: 'width=device-width, initial-scale=1, shrink-to-fit=no'
      },
      { name: 'theme-color', content: '#4285f4' },
      { hid: 'description', name: 'description', content: pkg.description }
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }]
  },

  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#49496a' },

  /*
  ** Global CSS
  */
  css: [
    'element-ui/lib/theme-chalk/index.css',
    'bulma/css/bulma.min.css',
    '@/assets/styles/primary.css',
    '@/assets/styles/extra.css',
    'vue-material-design-icons/styles.css'
  ],

  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    { src: '~/plugins/element-ui' },
    { src: '~/plugins/firebase/config' },
    { src: '~/plugins/vue-chartkick' },
    { src: '~/plugins/vue-material-design-icons', ssr: false },
    { src: '~/plugins/dateTimeFormatter' }
  ],

  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://github.com/nuxt-community/axios-module#usage
    '@nuxtjs/axios',
    [
      'nuxt-fire',
      {
        config: {
          development: {
            apiKey: 'AIzaSyBmHkjqWrnXkk2JSxwN3LyCXZ2FHE_G-Vw',
            authDomain: 'gyruproject.firebaseapp.com',
            databaseURL: 'https://gyruproject.firebaseio.com',
            projectId: 'gyruproject',
            storageBucket: 'gyruproject.appspot.com',
            messagingSenderId: '110906437389'
          },
          production: {
            apiKey: 'AIzaSyBmHkjqWrnXkk2JSxwN3LyCXZ2FHE_G-Vw',
            authDomain: 'gyruproject.firebaseapp.com',
            databaseURL: 'https://gyruproject.firebaseio.com',
            projectId: 'gyruproject',
            storageBucket: 'gyruproject.appspot.com',
            messagingSenderId: '110906437389'
          }
        }
      }
    ]
  ],
  /*
  ** Axios module configuration
  */
  axios: {
    // See https://github.com/nuxt-community/axios-module#options
  },

  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */

    extend(config, ctx) {}
  }
}
