import Vue from 'vue'
import VueChartkick from 'vue-chartkick'
import Highcharts from 'highcharts'

Vue.use(VueChartkick, { adapter: Highcharts })
