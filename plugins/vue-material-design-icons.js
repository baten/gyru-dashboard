import Vue from 'vue'

import ViewDashboardIcon from 'vue-material-design-icons/ViewDashboard.vue'
import DriversIcon from 'vue-material-design-icons/Car.vue'
import AccountChildCircleIcon from 'vue-material-design-icons/AccountChildCircle.vue'
import SessionIcon from 'vue-material-design-icons/TimerSand.vue'
import AppointmentIcon from 'vue-material-design-icons/Timer.vue'
import PaymentIcon from 'vue-material-design-icons/CardText.vue'
import SettingsIcon from 'vue-material-design-icons/Settings.vue'
import DeleteIcon from 'vue-material-design-icons/TrashCan.vue'
import EditIcon from 'vue-material-design-icons/Pencil.vue'
import LocationIcon from 'vue-material-design-icons/MapMarker.vue'
import AlertOctagon from 'vue-material-design-icons/AlertOctagon.vue'

Vue.component('view-dashboard-icon', ViewDashboardIcon)
Vue.component('driver-icon', DriversIcon)
Vue.component('student-icon', AccountChildCircleIcon)
Vue.component('session-icon', SessionIcon)
Vue.component('appointment-icon', AppointmentIcon)
Vue.component('payment-icon', PaymentIcon)
Vue.component('settings-icon', SettingsIcon)
Vue.component('delete-icon', DeleteIcon)
Vue.component('edit-icon', EditIcon)
Vue.component('location-icon', LocationIcon)
Vue.component('report-icon', AlertOctagon)
