import { Notification } from 'element-ui'

export const state = () => ({ drivers: [], postCodes: [] })
export const mutations = {
  setDriversData(state, driversDbData) {
    const driversDataArray = []
    for (let key in driversDbData) {
      driversDataArray.push({ ...driversDbData[key], id: key })
    }
    state.drivers = driversDataArray
  },
  setPostCodes(state, postCodeData) {
    const postCodesArray = []
    for (let key in postCodeData) {
      postCodesArray.push(postCodeData[key].postCode)
    }
    state.postCodes = postCodesArray
  },
  changeTrainerStatus(state, changeTrainerStatus) {
    const { trainerId, trainerUpdatedStatus } = changeTrainerStatus
    const findIndex = state.drivers.findIndex(
      trainer => trainer.id === trainerId
    )
    state.drivers[findIndex].status = trainerUpdatedStatus
  }
}
export const actions = {
  async getDriversData({ commit, state }, driversQueryOptions) {
    const endDurationDate = Number(new Date().getTime() / 1000)
    state.drivers.splice(0, state.drivers.length)
    let driversDbRef
    if (driversQueryOptions) {
      if (
        driversQueryOptions.postCode &&
        !driversQueryOptions.startDurationDate
      ) {
        driversDbRef = this.$fireDb
          .ref('trainers')
          .orderByChild('postCode')
          .equalTo(driversQueryOptions.postCode)
      } else if (
        driversQueryOptions.postCode &&
        driversQueryOptions.startDurationDate
      ) {
        driversDbRef = this.$fireDb
          .ref('trainers')
          .orderByChild('createdAt')
          .startAt(driversQueryOptions.startDurationDate)
        // .equalTo(driversQueryOptions.postCode)
      } else {
        driversDbRef = this.$fireDb.ref('trainers')
      }
    } else {
      driversDbRef = this.$fireDb.ref('trainers')
    }
    try {
      const snapshot = await driversDbRef.once('value')
      const driversDbData = snapshot.val()
      commit('setDriversData', driversDbData)
    } catch (e) {
      // alert(e)
      return
    }
  },
  async fetchPostCodes({ commit, state }) {
    state.postCodes.splice(0, state.postCodes.length)
    const driversDbRef = this.$fireDb.ref('trainers')
    try {
      const snapshot = await driversDbRef.once('value')
      const postCodeData = snapshot.val()
      commit('setPostCodes', postCodeData)
    } catch (e) {
      // alert(e)
      return
    }
  },
  updateTrainerStatus({ commit, state }, changeTrainerStatus) {
    const { trainerId, trainerUpdatedStatus } = changeTrainerStatus
    const driversDbRef = this.$fireDb.ref('trainers/' + trainerId)
    driversDbRef.update({ status: trainerUpdatedStatus }, function(error) {
      if (error) {
        Notification({
          title: 'Error',
          message: "Could't update status",
          type: 'error'
        })
      } else {
        commit('changeTrainerStatus', changeTrainerStatus)

        Notification({
          title: 'Success',
          message: `Trainer ${trainerUpdatedStatus} successfully`,
          type: 'success'
        })
      }
    })
  }
}
export const getters = {
  drivers(state) {
    return state.drivers
  },
  postCodes(state) {
    return state.postCodes
  }
}
