export const state = () => ({
  reportsAgainstTrainer: [],
  reportsAgainstStudent: [],
  reportsBug: [],
  reportOthers: []
})
export const mutations = {
  async setReportsAgainstTrainerData(state, reportsDbData) {
    const reportsDataArray = []
    for (let key in reportsDbData) {
      reportsDataArray.push({
        ...reportsDbData[key],
        id: key,
        reportedStudentName: null,
        reportedTrainerName: null
      })
    }

    try {
      reportsDataArray.forEach(report => {
        const studentName = new Promise((resolve, reject) => {
          this.$fireDb
            .ref()
            .child('students')
            .child(report.reportedByID)
            .on('value', student => {
              report.reportedStudentName = student.val().name
            })
          resolve()
        })

        const trainerName = new Promise((resolve, reject) => {
          this.$fireDb
            .ref()
            .child('trainers')
            .child(report.reportedAgainstID)
            .on('value', trainer => {
              report.reportedTrainerName = trainer.val().name
            })
          resolve()
        })

        Promise.all([studentName, trainerName]).then(() => {
          state.reportsAgainstTrainer = reportsDataArray
        })
      })
    } catch (error) {
      console.log(error)
    }
  },
  async setReportsAgainstStudentData(state, reportsDbData) {
    const reportsDataArray = []
    for (let key in reportsDbData) {
      reportsDataArray.push({
        ...reportsDbData[key],
        id: key,
        reportedStudentName: null,
        reportedTrainerName: null
      })
    }

    try {
      reportsDataArray.forEach(report => {
        const studentName = new Promise((resolve, reject) => {
          this.$fireDb
            .ref()
            .child('students')
            .child(report.reportedAgainstID)
            .on('value', student => {
              report.reportedStudentName = student.val().name
            })
          resolve()
        })

        const trainerName = new Promise((resolve, reject) => {
          this.$fireDb
            .ref()
            .child('trainers')
            .child(report.reportedByID)
            .on('value', trainer => {
              report.reportedTrainerName = trainer.val().name
            })
          resolve()
        })

        Promise.all([studentName, trainerName]).then(() => {
          state.reportsAgainstStudent = reportsDataArray
        })
      })
    } catch (error) {
      console.log(error)
    }
    // state.reportsAgainstStudent = reportsDataArray
  },
  async setBugReportsData(state, reportsDbData) {
    const reportsDataArray = []
    for (let key in reportsDbData) {
      reportsDataArray.push({
        ...reportsDbData[key],
        id: key,
        reportedUserName: null
      })
    }
    try {
      reportsDataArray.forEach(report => {
        const userName = new Promise((resolve, reject) => {
          this.$fireDb
            .ref()
            .child('students')
            .child(report.reportedByID)
            .on('value', student => {
              if (student.val() !== null) {
                report.reportedUserName = student.val().name
              } else {
                this.$fireDb
                  .ref()
                  .child('trainers')
                  .child(report.reportedByID)
                  .on('value', trainer => {
                    if (trainer.val() !== null) {
                      report.reportedUserName = trainer.val().name
                    }
                  })
              }
            })
          resolve()
        })

        userName.then(() => {
          state.reportsBug = reportsDataArray
        })
      })
    } catch (error) {
      console.log(error)
    }
  },
  async setOtherReportsData(state, reportsDbData) {
    const reportsDataArray = []
    for (let key in reportsDbData) {
      reportsDataArray.push({
        ...reportsDbData[key],
        id: key,
        reportedUserName: null
      })
    }
    try {
      reportsDataArray.forEach(report => {
        const userName = new Promise((resolve, reject) => {
          this.$fireDb
            .ref()
            .child('students')
            .child(report.reportedByID)
            .on('value', student => {
              if (student.val() !== null) {
                report.reportedUserName = student.val().name
              } else {
                this.$fireDb
                  .ref()
                  .child('trainers')
                  .child(report.reportedByID)
                  .on('value', trainer => {
                    if (trainer.val() !== null) {
                      report.reportedUserName = trainer.val().name
                    }
                  })
              }
            })
          resolve()
        })

        userName.then(() => {
          state.reportOthers = reportsDataArray
        })
      })
    } catch (error) {
      console.log(error)
    }
  }
}
export const actions = {
  async getReportsAgainstTrainerData({ commit, state }) {
    state.reportsAgainstTrainer.splice(0, state.reportsAgainstTrainer.length)
    let reportsDbRef
    reportsDbRef = this.$fireDb
      .ref('reports')
      .child('againstTrainer')
      .orderByChild('createdAt')
    try {
      const snapshot = await reportsDbRef.once('value')
      const reportsDbData = snapshot.val()
      commit('setReportsAgainstTrainerData', reportsDbData)
    } catch (e) {
      // alert(e)
      return
    }
  },
  async getReportsAgainstStudentData({ commit, state }) {
    state.reportsAgainstStudent.splice(0, state.reportsAgainstStudent.length)
    let studentReportsDbRef
    studentReportsDbRef = this.$fireDb
      .ref('reports')
      .child('againstStudent')
      .orderByChild('createdAt')
    try {
      const snapshot = await studentReportsDbRef.once('value')
      const reportsDbData = snapshot.val()
      commit('setReportsAgainstStudentData', reportsDbData)
    } catch (e) {
      // alert(e)
      return
    }
  },
  async getBugReportsData({ commit, state }) {
    state.reportsBug.splice(0, state.reportsBug.length)
    let studentReportsDbRef
    studentReportsDbRef = this.$fireDb
      .ref('reports')
      .child('bug')
      .orderByChild('createdAt')
    try {
      const snapshot = await studentReportsDbRef.once('value')
      const reportsDbData = snapshot.val()
      commit('setBugReportsData', reportsDbData)
    } catch (e) {
      // alert(e)
      return
    }
  },
  async getOtherReportsData({ commit, state }) {
    state.reportOthers.splice(0, state.reportOthers.length)
    let studentReportsDbRef
    studentReportsDbRef = this.$fireDb
      .ref('reports')
      .child('other')
      .orderByChild('createdAt')
    try {
      const snapshot = await studentReportsDbRef.once('value')
      const reportsDbData = snapshot.val()
      commit('setOtherReportsData', reportsDbData)
    } catch (e) {
      // alert(e)
      return
    }
  }
}
export const getters = {
  reportsAgainstTrainer(state) {
    return state.reportsAgainstTrainer
  },
  reportsAgainstStudent(state) {
    return state.reportsAgainstStudent
  },
  reportsBug(state) {
    return state.reportsBug
  },
  reportOthers(state) {
    return state.reportOthers
  }
}
