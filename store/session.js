export const state = () => ({ sessions: [] })
export const mutations = {
  setSessionData(state, sessionsDbData) {
    const sessionsDataArray = []
    for (let key in sessionsDbData) {
      sessionsDataArray.push({ ...sessionsDbData[key], id: key })
    }
    state.sessions = sessionsDataArray
  }
}
export const actions = {
  async getSessionData({ commit, state }) {
    state.sessions.splice(0, state.sessions.length)
    let sessionsDbRef = this.$fireDb.ref('sessions').orderByChild('createdAt')

    try {
      const snapshot = await sessionsDbRef.once('value')
      const sessionsDbData = snapshot.val()
      commit('setSessionData', sessionsDbData)
    } catch (e) {
      // alert(e)
      return
    }
  }
}
export const getters = {
  sessions(state) {
    return state.sessions
  }
}
